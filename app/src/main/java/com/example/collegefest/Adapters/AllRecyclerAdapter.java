package com.example.collegefest.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.collegefest.Models.ModelResponse;
import com.example.collegefest.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AllRecyclerAdapter extends RecyclerView.Adapter<AllRecyclerAdapter.Holder> {
    ArrayList<ModelResponse> arrayList;
    Context context;
    public AllRecyclerAdapter(ArrayList<ModelResponse> arrayList, FragmentActivity activity) {
        this.arrayList=arrayList;
        this.context=activity;
    }

    @NonNull
    @Override
    public AllRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_recycler, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllRecyclerAdapter.Holder holder, int i) {

        holder.product_name.setText(arrayList.get(i).getP_name());
        Picasso.with(context).load(arrayList.get(i).getImage()).into(holder.product_img);


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        ImageView product_img;
        TextView product_name;

        public Holder(@NonNull View itemView) {
            super(itemView);
            product_name=itemView.findViewById(R.id.product_name);
            product_img=itemView.findViewById(R.id.product_img);
        }
    }
}
