package com.example.collegefest.Models;

public class ModelResponse {
    int image;
    String p_name;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public ModelResponse(int image, String p_name) {
        this.image = image;
        this.p_name = p_name;
    }
}
