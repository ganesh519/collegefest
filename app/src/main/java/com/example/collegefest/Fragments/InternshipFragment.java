package com.example.collegefest.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.collegefest.Adapters.InternshipRecyclerAdapter;
import com.example.collegefest.Adapters.WorkShopRecyclerAdapter;
import com.example.collegefest.Models.ModelResponse;
import com.example.collegefest.R;

import java.util.ArrayList;

public class InternshipFragment extends Fragment {
    RecyclerView recycler_all;
    ArrayList<ModelResponse> arrayList=new ArrayList<>();
    InternshipRecyclerAdapter internshipRecyclerAdapter;
    RecyclerView.LayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_all, container, false);

        recycler_all=v.findViewById(R.id.recycler_all);

        arrayList.add(new ModelResponse(R.drawable.internships,"Events"));
        arrayList.add(new ModelResponse(R.drawable.internships,"Events"));
        arrayList.add(new ModelResponse(R.drawable.internships,"Events"));
        arrayList.add(new ModelResponse(R.drawable.internships,"Events"));
        arrayList.add(new ModelResponse(R.drawable.internships,"Events"));
        arrayList.add(new ModelResponse(R.drawable.internships,"Events"));
        arrayList.add(new ModelResponse(R.drawable.internships,"Events"));
        arrayList.add(new ModelResponse(R.drawable.internships,"Events"));
        arrayList.add(new ModelResponse(R.drawable.internships,"Events"));


        internshipRecyclerAdapter=new InternshipRecyclerAdapter(arrayList,getActivity());
        layoutManager=new LinearLayoutManager(getActivity());
        recycler_all.setNestedScrollingEnabled(false);
        recycler_all.setLayoutManager(layoutManager);
        recycler_all.setAdapter(internshipRecyclerAdapter);
        return v;
    }
}
