package com.example.collegefest.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.collegefest.Adapters.InternshipRecyclerAdapter;
import com.example.collegefest.Adapters.JobsRecyclerAdapter;
import com.example.collegefest.Models.ModelResponse;
import com.example.collegefest.R;

import java.util.ArrayList;

public class JobsFragment extends Fragment {
    RecyclerView recycler_all;
    ArrayList<ModelResponse> arrayList=new ArrayList<>();
    JobsRecyclerAdapter jobsRecyclerAdapter;
    RecyclerView.LayoutManager layoutManager;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_all, container, false);

        recycler_all=v.findViewById(R.id.recycler_all);

        arrayList.add(new ModelResponse(R.drawable.jobs,"Events"));
        arrayList.add(new ModelResponse(R.drawable.jobs,"Events"));
        arrayList.add(new ModelResponse(R.drawable.jobs,"Events"));
        arrayList.add(new ModelResponse(R.drawable.jobs,"Events"));
        arrayList.add(new ModelResponse(R.drawable.jobs,"Events"));
        arrayList.add(new ModelResponse(R.drawable.jobs,"Events"));
        arrayList.add(new ModelResponse(R.drawable.jobs,"Events"));
        arrayList.add(new ModelResponse(R.drawable.jobs,"Events"));
        arrayList.add(new ModelResponse(R.drawable.jobs,"Events"));
        jobsRecyclerAdapter=new JobsRecyclerAdapter(arrayList,getActivity());
        layoutManager=new LinearLayoutManager(getActivity());
        recycler_all.setNestedScrollingEnabled(false);
        recycler_all.setLayoutManager(layoutManager);
        recycler_all.setAdapter(jobsRecyclerAdapter);

        return v;
    }
}
